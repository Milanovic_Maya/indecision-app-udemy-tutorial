"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IndecisionApp = function (_React$Component) {
    _inherits(IndecisionApp, _React$Component);

    function IndecisionApp(props) {
        _classCallCheck(this, IndecisionApp);

        var _this = _possibleConstructorReturn(this, (IndecisionApp.__proto__ || Object.getPrototypeOf(IndecisionApp)).call(this, props));

        _this.state = {
            options: props.options,
            title: "Indecision App"
        };
        _this.getOption = _this.getOption.bind(_this);
        _this.deleteOptions = _this.deleteOptions.bind(_this);
        _this.onUserPressButton = _this.onUserPressButton.bind(_this);
        _this.onDeleteSingleOption = _this.onDeleteSingleOption.bind(_this);
        return _this;
    }

    _createClass(IndecisionApp, [{
        key: "getOption",
        value: function getOption(option) {
            if (!option) {
                return "Enter valid option!";
            } else if (this.state.options.includes(option)) {
                return "Option already exist!";
            }
            //this.setState() gets two arguments:
            // callback1: (prevState, currentProps),
            // callback2: that executes after state updates;
            this.setState(function (prevState) {
                return {
                    options: [].concat(_toConsumableArray(prevState.options), [option])
                };
            });
            // const { options } = this.state;
            // this.setState({ options: [...options, option] })
        }
    }, {
        key: "deleteOptions",
        value: function deleteOptions() {
            this.setState(function () {
                return { options: [] };
            });
        }
    }, {
        key: "onUserPressButton",
        value: function onUserPressButton() {
            var randomNum = Math.floor(Math.random() * this.state.options.length);
            var option = this.state.options[randomNum];
            alert(option);
        }
    }, {
        key: "onDeleteSingleOption",
        value: function onDeleteSingleOption(optionToRemove) {
            this.setState(function (prevState) {
                return {
                    options: prevState.options.filter(function (existingOption) {
                        return optionToRemove !== existingOption;
                    })
                };
            });
        }
    }, {
        key: "componentDidMount",
        value: function componentDidMount() {

            try {
                var json = localStorage.getItem("options");
                var options = JSON.parse(json);
                if (options) {
                    this.setState(function () {
                        return { options: options };
                    });
                }
            } catch (e) {
                //do nothng at all, but UI will not crash
            }
        }
    }, {
        key: "componentDidUpdate",
        value: function componentDidUpdate(prevProps, prevState) {
            if (prevState.options.length !== this.state.options.length) {
                var json = JSON.stringify(this.state.options);
                localStorage.setItem("options", json);
                console.log("updated,saving data");
            }
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            console.log("unmounted");
        }
    }, {
        key: "render",
        value: function render() {
            var _state = this.state,
                options = _state.options,
                title = _state.title;

            return React.createElement(
                "div",
                null,
                React.createElement(Header, { title: title }),
                React.createElement(Action, { hasOptions: options.length > 0,
                    onUserPressButton: this.onUserPressButton }),
                React.createElement(Options, { options: options,
                    deleteOptions: this.deleteOptions,
                    onDeleteSingleOption: this.onDeleteSingleOption }),
                React.createElement(AddOptions, { getOption: this.getOption })
            );
        }
    }]);

    return IndecisionApp;
}(React.Component);

IndecisionApp.defaultProps = {
    options: []
};

var Header = function Header(props) {
    return React.createElement(
        "div",
        null,
        React.createElement(
            "h1",
            null,
            props.title
        )
    );
};
Header.defaultProps = {
    title: "App title"
};

var Action = function Action(props) {
    var onUserClick = function onUserClick() {
        props.onUserPressButton();
    };
    var hasOptions = props.hasOptions;

    return React.createElement(
        "div",
        null,
        React.createElement(
            "button",
            { disabled: !hasOptions,
                onClick: onUserClick },
            "Choose one"
        )
    );
};

var Options = function Options(props) {
    var options = props.options,
        deleteOptions = props.deleteOptions,
        onDeleteSingleOption = props.onDeleteSingleOption;

    return React.createElement(
        "div",
        null,
        React.createElement(
            "button",
            { onClick: deleteOptions },
            "Remove all"
        ),
        options.length === 0 && React.createElement(
            "p",
            null,
            "Please add an option"
        ),
        "Options:",
        options.length,
        options.map(function (option) {
            return React.createElement(Option, {
                onDeleteSingleOption: onDeleteSingleOption,
                key: option,
                option: option });
        }),
        React.createElement(Option, null)
    );
};

var Option = function Option(props) {
    var option = props.option,
        onDeleteSingleOption = props.onDeleteSingleOption;

    var deleteOption = function deleteOption(event) {
        onDeleteSingleOption(option);
    };
    return React.createElement(
        "div",
        null,
        option,
        option && React.createElement(
            "button",
            { onClick: deleteOption },
            "remove"
        )
    );
};

var AddOptions = function (_React$Component2) {
    _inherits(AddOptions, _React$Component2);

    function AddOptions(props) {
        _classCallCheck(this, AddOptions);

        var _this2 = _possibleConstructorReturn(this, (AddOptions.__proto__ || Object.getPrototypeOf(AddOptions)).call(this, props));

        _this2.state = { input: "", error: "" };
        _this2.OnFormSubmit = _this2.OnFormSubmit.bind(_this2);
        _this2.onInputChange = _this2.onInputChange.bind(_this2);
        return _this2;
    }

    _createClass(AddOptions, [{
        key: "onInputChange",
        value: function onInputChange(event) {
            var input = event.target.value.trim();
            this.setState({ input: input });
        }
    }, {
        key: "OnFormSubmit",
        value: function OnFormSubmit(event) {
            event.preventDefault();
            var option = event.target.elements.option.value.trim();

            var error = this.props.getOption(option);
            this.setState({ input: "" });

            this.setState(function () {
                return { error: error };
            });
            if (!error) {
                event.target.elements.option.value = "";
            }
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                this.state.error && React.createElement(
                    "p",
                    null,
                    this.state.error
                ),
                React.createElement(
                    "form",
                    { onSubmit: this.OnFormSubmit },
                    React.createElement("input", { value: this.state.input, onChange: this.onInputChange, type: "text", name: "option", id: "" }),
                    React.createElement(
                        "button",
                        { type: "submit" },
                        "Add Option"
                    )
                )
            );
        }
    }]);

    return AddOptions;
}(React.Component);

var appRoot = document.getElementById("app");
ReactDOM.render(React.createElement(IndecisionApp, { options: [] }), appRoot);
