import React from "react";
import Option from "./Option";

const Options = (props) => {
    const { options, deleteOptions, onDeleteSingleOption } = props;
    return (
        <div>
            <div className="widget-header">
                <h3 className="widget-header__title">Your options</h3>
                <button
                    className="button button--link"
                    onClick={deleteOptions} >
                    Remove all
        </button>
            </div>
            {options.length === 0 &&
                <p className="widget__message">Please add an option</p>}
            {options.map((option, index) => {
                return <Option
                    onDeleteSingleOption={onDeleteSingleOption}
                    key={option}
                    count={index + 1}
                    option={option} />
            })}
            <Option />
        </div>
    )
}

export default Options;