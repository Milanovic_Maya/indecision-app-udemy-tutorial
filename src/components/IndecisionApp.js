import React from "react";

import AddOptions from "./AddOptions";
import Options from "./Options";
import Action from "./Action";
import Header from "./Header";
import OptionModal from "./OptionModal";


class IndecisionApp extends React.Component {
    state = {
        options: [],
        selectedOption: undefined,
        title: "Indecision",
        subtitle: "Make a choice"
    };

    getOption = (option) => {
        if (!option) {
            return "Enter valid option!"
        } else if (this.state.options.includes(option)) {
            return "Option already exist!"
        }
        this.setState((prevState) => {
            return {
                options: [...prevState.options, option]
            }
        })
    }
    deleteOptions = () => {
        this.setState(() => ({ options: [] }))
    }

    onUserPressButton = () => {
        const randomNum = Math.floor(Math.random() * this.state.options.length);
        const option = this.state.options[randomNum];
        //modal here:react-modal
        this.setState(() => ({ selectedOption: option }));
    }

    onDeleteSingleOption = (optionToRemove) => {
        this.setState(prevState => ({
            options: prevState.options.filter(existingOption => {
                return optionToRemove !== existingOption;
            })
        }))
    }

    clearSelectedOption = () => {
        this.setState(() => ({ selectedOption: undefined }))
    }

    componentDidMount() {

        try {
            const json = localStorage.getItem("options");
            const options = JSON.parse(json);
            if (options) {
                this.setState(() => ({ options }));
            }
        } catch (e) {
            //do nothng at all, but UI will not crash
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevState.options.length !== this.state.options.length) {
            const json = JSON.stringify(this.state.options);
            localStorage.setItem("options", json);
            console.log("updated,saving data");
        }
    }
    componentWillUnmount() {
        console.log("unmounted");
    }

    render() {
        const { options, title, subtitle, selectedOption } = this.state;
        return (
            <div>
                <Header title={title} subtitle={subtitle} />
                <div className="container">
                    <Action hasOptions={options.length > 0}
                        onUserPressButton={this.onUserPressButton} />
                    <div className="widget">
                        <Options options={options}
                            deleteOptions={this.deleteOptions}
                            onDeleteSingleOption={this.onDeleteSingleOption} />
                        <AddOptions getOption={this.getOption} />
                    </div>
                </div>
                <OptionModal
                    selectedOption={selectedOption}
                    clearSelectedOption={this.clearSelectedOption}
                />
            </div>
        )
    }
}

IndecisionApp.defaultProps = {
    options: []
}

export default IndecisionApp;