import React from "react";
import Modal from "react-modal";

Modal.setAppElement('#app');

const OptionModal = props => {
    const { selectedOption, clearSelectedOption } = props;
    return (

        <Modal
            isOpen={!!selectedOption}
            onRequestClose={clearSelectedOption}
            //onRequestClose calls callback function when:
            //  user tries to close modal esc/background click
            contentLabel="Selected Option"
            closeTimeoutMS={200}
            className="modal"
        >
            <h3 className="modal__title">Selected option</h3>
            {selectedOption &&
                <p className="modal__body">{selectedOption}</p>}
            <button
                className="button"
                onClick={clearSelectedOption}>Ok</button>
        </Modal>

    );
}

export default OptionModal;