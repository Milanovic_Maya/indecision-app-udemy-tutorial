import React from "react";
const Option = (props) => {
    const { option, onDeleteSingleOption, count } = props;
    const deleteOption = (event) => {
        onDeleteSingleOption(option);
    }

    return (
        <div>
            {option &&
                <div className="option">
                    <p className="option__text">{count}.{option}</p>
                    {option &&
                        <button
                            className="button button--link"
                            onClick={deleteOption}>
                            remove
            </button>}
                </div>
            }
        </div>
    )
}

export default Option;