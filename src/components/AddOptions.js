
import React from "react";

class AddOptions extends React.Component {
    constructor(props) {
        super(props);
        this.state = { input: "", error: "" }
        this.OnFormSubmit = this.OnFormSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    onInputChange(event) {
        const input = event.target.value.trim();
        this.setState({ input });
    }

    OnFormSubmit(event) {
        event.preventDefault();
        const option = event.target.elements.option.value.trim();

        const error = this.props.getOption(option);
        this.setState({ input: "" });

        this.setState(() => ({ error }));
        if (!error) {
            event.target.elements.option.value = "";
        }
    }
    render() {
        return (
            <div>
                {
                    (this.state.error &&
                        <p className="add-option-error">{this.state.error}</p>)
                }
                <form
                    className="add-option"
                    onSubmit={this.OnFormSubmit}>
                    <input
                        className="add-option__input"
                        value={this.state.input} onChange={this.onInputChange} type="text" name="option" id="" />
                    <button
                        className="button"
                        type="submit">
                        Add Option
                    </button>
                </form>
            </div>
        )
    }
};

export default AddOptions;