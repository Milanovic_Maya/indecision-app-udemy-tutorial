import React from "react";
const Action = (props) => {
    const onUserClick = () => {
        props.onUserPressButton();
    }
    const { hasOptions } = props;
    return (
        <div>
            <button
                className="big-button"
                disabled={!hasOptions}
                onClick={onUserClick}>What should I do?</button>
        </div>
    )
}
export default Action;