class IndecisionApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: props.options,
            title: "Indecision App"
        };
        this.getOption = this.getOption.bind(this);
        this.deleteOptions = this.deleteOptions.bind(this);
        this.onUserPressButton = this.onUserPressButton.bind(this);
        this.onDeleteSingleOption = this.onDeleteSingleOption.bind(this);
    }

    getOption(option) {
        if (!option) {
            return "Enter valid option!"
        } else if (this.state.options.includes(option)) {
            return "Option already exist!"
        }
        //this.setState() gets two arguments:
        // callback1: (prevState, currentProps),
        // callback2: that executes after state updates;
        this.setState((prevState) => {
            return {
                options: [...prevState.options, option]
            }
        })
        // const { options } = this.state;
        // this.setState({ options: [...options, option] })
    }
    deleteOptions() {
        this.setState(() => ({ options: [] }))
    }

    onUserPressButton() {
        const randomNum = Math.floor(Math.random() * this.state.options.length);
        const option = this.state.options[randomNum];
        alert(option);
    }

    onDeleteSingleOption(optionToRemove) {
        this.setState(prevState => ({
            options: prevState.options.filter(existingOption => {
                return optionToRemove !== existingOption;
            })
        }))
    }

    componentDidMount() {

        try {
            const json = localStorage.getItem("options");
            const options = JSON.parse(json);
            if (options) {
                this.setState(() => ({ options }));
            }
        } catch (e) {
            //do nothng at all, but UI will not crash
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevState.options.length !== this.state.options.length) {
            const json = JSON.stringify(this.state.options);
            localStorage.setItem("options", json);
            console.log("updated,saving data");
        }
    }
    componentWillUnmount() {
        console.log("unmounted");
    }

    render() {
        const { options, title } = this.state;
        return (
            <div>
                <Header title={title} />
                <Action hasOptions={options.length > 0}
                    onUserPressButton={this.onUserPressButton} />
                <Options options={options}
                    deleteOptions={this.deleteOptions}
                    onDeleteSingleOption={this.onDeleteSingleOption} />
                <AddOptions getOption={this.getOption} />
            </div>
        )
    }
}

IndecisionApp.defaultProps = {
    options: []
}

const Header = (props) => {
    return (
        <div>
            <h1>{props.title}</h1>
        </div>
    );
};
Header.defaultProps = {
    title: "App title"
}



const Action = (props) => {
    const onUserClick = () => {
        props.onUserPressButton();
    }
    const { hasOptions } = props;
    return (
        <div>
            <button disabled={!hasOptions}
                onClick={onUserClick}>Choose one</button>
        </div>
    )
}

const Options = (props) => {
    const { options, deleteOptions, onDeleteSingleOption } = props;
    return (
        <div>
            <button onClick={deleteOptions} >Remove all</button>
            {options.length===0 && <p>Please add an option</p>}
            Options:{options.length}
            {options.map(option => {
                return <Option
                    onDeleteSingleOption={onDeleteSingleOption}
                    key={option}
                    option={option} />
            })}
            <Option />
        </div>
    )
}

const Option = (props) => {
    const { option, onDeleteSingleOption } = props;
    const deleteOption = (event) => {
        onDeleteSingleOption(option);
    }
    return (
        <div>
            {option}
            {option && <button onClick={deleteOption}>remove</button>}
        </div>
    )
}

class AddOptions extends React.Component {
    constructor(props) {
        super(props);
        this.state = { input: "", error: "" }
        this.OnFormSubmit = this.OnFormSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    onInputChange(event) {
        const input = event.target.value.trim();
        this.setState({ input });
    }

    OnFormSubmit(event) {
        event.preventDefault();
        const option = event.target.elements.option.value.trim();

        const error = this.props.getOption(option);
        this.setState({ input: "" });

        this.setState(() => ({ error }));
        if (!error) {
            event.target.elements.option.value = "";
        }
    }
    render() {
        return (
            <div>
                {
                    (this.state.error && <p>{this.state.error}</p>)
                }
                <form onSubmit={this.OnFormSubmit}>
                    <input value={this.state.input} onChange={this.onInputChange} type="text" name="option" id="" />
                    <button type="submit">Add Option</button>
                </form>
            </div>
        )
    }
}


const appRoot = document.getElementById("app");
ReactDOM.render(<IndecisionApp options={[]} />, appRoot);


