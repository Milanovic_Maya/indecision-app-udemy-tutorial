
console.log('App.js is running!');


let count = 0;
const addOne = () => {
    count += 1;
    renderCounterApp();//render app with changes!!!!
};
const minusOne = () => {
    count--;
    renderCounterApp();
};
const reset = () => {
    count = 0;
    renderCounterApp();
};


// console.log(template);//jsx=>js object

const appRoot = document.getElementById('app');


const renderCounterApp = function () {
    //this is similar how react works!

    //jsx:
    const template = (
        <div>
            <h1>Count: {count}</h1>
            <button onClick={addOne} >+1</button>
            <button onClick={minusOne} >-1</button>
            <div>
                <button onClick={reset}>reset</button></div>
        </div>
    )

    //render that jsx-only part that is modified:
    ReactDOM.render(template, appRoot);
};

renderCounterApp();//init app
