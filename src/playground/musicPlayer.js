//visibility toggle task:


let toggler = false;

const onUserClick = () => {
    toggler = !toggler;
    renderPage();
}

const renderPage = function () {
    const appRoot = document.getElementById('app');

    const app = (

        <div>
            <h1>Play some music!</h1>
            {
                (toggler)
                    ? <div>
                        <button onClick={onUserClick}>Stop</button>
                        <p>Music is on</p>
                        <audio controls autoPlay>
                            <source src="Hanggai_-_The_Vast_Grassland.mp3" type="audio/mp3" />
                        </audio>
                    </div>

                    : <button onClick={onUserClick}>Play</button>
            }
        </div>

    );
    ReactDOM.render(app, appRoot);
}
renderPage();
