//arguments objects and this are no longer bound with arrow function

const add = (a, b) => {
    //in arrow function, arguments is not defined!
    // console.log(arguments);//ref error
    return a + b
};
// console.log(add(1, 2));

const user = {
    name: "LakiPaki",
    cities: ["Bg", "Pa"],
    printPlacesLived() {
        console.log(this.name);
        console.log(this.cities);
        return this.cities.map(city => {
            console.log(`${this.name} lived in ${city}.`);
            //prints error with ES5 function call in forEach(func(){})
            //because we don't have object that is calling that callback
            return city.toUpperCase();
        });
    }
}

// console.log(user.printPlacesLived());


const multiplier = {
    numbers: [1, 2, 3],
    singleNum: 2,
    multiply() {
        return this.numbers.map(num => num * this.singleNum);
    }
}
console.log(multiplier.multiply());