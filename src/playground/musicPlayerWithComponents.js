//////////////////////////////////////////////////

//create Component MusicPlayer
//methods render(), constructor(), onClickMethod()
//state: toggler:false

class MusicPlayer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toggler: false
        };
        this.musicToggler = this.musicToggler.bind(this);
    }

    musicToggler() {
        this.setState(prevState => {
            return { toggler: !prevState.toggler }
        })
    }

    render() {
        const { toggler } = this.state;
        return (
            <div>
                <h1>Music player </h1>
                {
                    (!toggler)
                        ? <button onClick={this.musicToggler}>Play</button>
                        : <div>
                            <button onClick={this.musicToggler} >Stop</button>
                            <p>Music is playing!</p>
                            <audio controls autoPlay>
                                <source src="Hanggai_-_The_Vast_Grassland.mp3" type="audio/mp3" />
                            </audio>
                        </div>
                }
            </div>
        )
    }
}

ReactDOM.render(<MusicPlayer />, document.getElementById("app"));