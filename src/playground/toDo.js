const appRoot = document.getElementById('app');

const appObj = {
    options: [],
    title: "My App"
}

const onFormSubmission = (event) => {
    event.preventDefault();
    const option = event.target.elements.option.value;

    if (option) {
        appObj.options.push(option);
        event.target.elements.option.value = "";
        //need to render changes
        renderMyApp();

    }
}

const removeAll = () => {
    appObj.options = [];
    renderMyApp();
};

const onChoice = () => {
    //generate random num
    const randomNum = Math.floor(Math.random() * appObj.options.length);
    let option = appObj.options[randomNum];
    // renderMyApp();
    alert(option);
};


const renderMyApp = function () {
    const app = (
        <div>
            <h3>{appObj.title}</h3>
            <button disabled={appObj.options.length === 0} onClick={onChoice}>Cat?</button>
            <button onClick={removeAll}>remove all</button>

            {//jsx supports arrays to be rendered, but jsx elems need key
                // ["Laki", "Paki", "Sekica",<p key="cats">cats</p>]
            }

            <ol>{
                (appObj.options.length !== 0) &&
                appObj.options.map(option => {
                    return <li key={option}>{option}</li>
                })}
            </ol>
            <form onSubmit={onFormSubmission}>
                <input type="text" name="option" />
                <button>add option</button>
            </form>
        </div>
    );

    ReactDOM.render(app, appRoot);
}

renderMyApp();

