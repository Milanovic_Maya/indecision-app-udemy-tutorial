var nameVar = "Maya";
var nameVar = "Laki_Paki";
//can be re-assigned and re-declared
// console.log("nameVar", nameVar);

let nameLet = "Laki";
//cannot be declared again
nameLet = "Paki";
// console.log("nameLet", nameLet);

const nameConst = "Cula";
//cannot be re-assigned and re-declared
// console.log("nameConst", nameConst);

//let and const are block-scoped;