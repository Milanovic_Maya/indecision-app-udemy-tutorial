//jsx - javascript xml

console.log('App.js is running!');

// JSX - JavaScript XML
var template = <h1>Indecision App</h1>;
var appRoot = document.getElementById('app');

ReactDOM.render(template, appRoot);


//we get error, code needs to be compiled to plain JS

//tool Babel.js compiles:
// - to React.createElement("div",attributes obj,content) and ES5

// .install Babel.js locally; npm install -g babel-cli 
//it doesn't do anything alone
//needs plugins and presets to run
//preset is group of plugins

//Presets:react, ES2015, env( for ES6 and ES7 features)
//install Babel(-g), env preset and React preset locally

//yarn init

//package.json
//install babel-preset-react & babel-preset-env
//they need own dependencies-node_modules

//run: this command compiles app.js from src to output in public/app.js
//babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch


console.log('App.js is running!');


let count = 0;
const addOne = () => {
    count += 1;
    renderCounterApp();//render app with changes!!!!
};
const minusOne = () => {
    count--;
    renderCounterApp();
};
const reset = () => {
    count = 0;
    renderCounterApp();
};


// console.log(template);//jsx=>js object

const appRoot = document.getElementById('app');


const renderCounterApp = function () {
    //this is similar how react works!

    //jsx:
    const template = (
        <div>
            <h1>Count: {count}</h1>
            <button onClick={addOne} >+1</button>
            <button onClick={minusOne} >-1</button>
            <div>
                <button onClick={reset}>reset</button></div>
        </div>
    )

    //render that jsx-only part that is modified:
    ReactDOM.render(template, appRoot);
};

renderCounterApp();//init app


